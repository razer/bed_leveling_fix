# Bed leveling Fix
Small python3 command line tool intent to fix auto leveling issues, most of the time due to squeezed X/Y axis.
You can set offsets on X and Y axes in your mesh point, with linear or bilinear correction.

## Step 1: Diagnose your auto bed leveling problem
This tool was designed to fix <b>mechanical issues</b>. All other possible issues have not been tested (for sure, if your probe is not consistant, this tool is useless). Then it's important to understand what issue you have.

The first thing to look closely is your <b>probe position</b>. The best way should usually be nearest the nozzle as possible, but sometimes it's better to find a position where one offset is near to 0. That way you should get only one axis causing issues.
In my case I have both axes squeezed, Y the most, and I choose to put my probe 60mm left of my nozzle but aligned with Y axis. Then only X axis have to be fixed.

Then, when you have found the best probe position for you, this method will check is your Z probe offset differs in some parts of X/Y axis. Since it can only be caused by squeeze axis, you will be sure is mechanical :
 - Take note the Z offset value you set in marlin (control -> motion)
 - Be sure to have Bed leveling disabled : 
  - `M420S0`
 - Position your nozzle in a zone where leveling problems occurs, then
   manually deploy your Z probe (or activate inductive ones)
  - `M401` or `M280 P0 S10`
 - Move the Z axis down, by 0.1 step at the end, until your probe detect your bed. Take note of the Z height and reset your probe
  - `M280 P0 S160`
  - `M280 P0 S90`
 - Move your nozzle to the position your probe was previously (according with your probe position)
 - Move the Z axis down, with a piece of paper on your bed, until you find the correct height. <b>Do the difference with the previous Z height (probe detection), then compare with your Z offset value from marlin conf (first step)</b>. If the values differs, bad news : your axis is squeeze, good news : this tool was designed for you

## Step 2: Get your printer ready
- Prepare marlin firmware (1.1 bugfix ideally) arduino project
- Enable Bilinear mesh leveling
 - `#define AUTO_BED_LEVELING_BILINEAR`
- Enable Save to EEPROM
 - `#define EEPROM_SETTINGS   // Enable for M500 and M501 commands`
 - `//#define DISABLE_M503    // Comment this line !`
- Be sure to put the right X and Y offset for your probe. <b>Note: values are here as example, you have to define your own</b>
 - `#define X_PROBE_OFFSET_FROM_EXTRUDER 60 // X offset: -left  +right  [of the nozzle]`
 - `#define Y_PROBE_OFFSET_FROM_EXTRUDER 0`
- Fix probe X/Y values to have a mesh centered with your bed. <b>Note: values are here as example, you have to define your own</b>
 - `#define LEFT_PROBE_BED_POSITION 30`
 - `#define RIGHT_PROBE_BED_POSITION 190`
 - `#define FRONT_PROBE_BED_POSITION 30`
 - `#define BACK_PROBE_BED_POSITION 180`
- Define left front bed part as Z_SAFE_HOMING
 - `#define Z_SAFE_HOMING`
 - `#define Z_SAFE_HOMING_X_POINT LEFT_PROBE_BED_POSITION    // X point for Z homing when homing all axes (G28).`
 - `#define Z_SAFE_HOMING_Y_POINT FRONT_PROBE_BED_POSITION   // Y point for Z homing when homing all axes (G28).`

## Step 3: clone this repo, and get dependencies right :
- python >= 3.5
- python-serial

If you want to handle octoprint, then also get:
- python-requests >= 2.18

## Step 4: edit config.py
- Printer port setup
- X and/or Y axis final offset(s) (see below) : X_FINAL_OFFSET, Y_FINAL_OFFSET
- X and/or Y axis intermediate offsets (see below) : X_POINT_OFFSET, Y_POINT_OFFSET
- Preheat settings
- Octoprint handling : it basicaly disconnect octoprint from printer before running leveling, and restore the connection once it's done

## How to set right offset values for axis
- The front left corner of the bed, used as Z_SAFE_HOMING (see Step 2), is the reference point for both axes. Logicaly the offset of this point will be 0, since you will set your Z probe offset (control -> motion -> Probe offset) from it
- Correction on both axes can be linear or bilinear
- Linear mean that the offset increase/decrease proportionnaly along the axe. As an example, if X_FINAL_OFFSET is set to 1.0 (+1mm), a mesh point in the center of the axe will have 0.5 as offset :
 - X_FINAL_OFFSET 1
 - X_POINT_OFFSET 0,0
 - 5 points mesh Y offsets will be : X0:0, X1:0.25, X2:0.5, X3:0.75, X4:0.1
- Bilinear mean that you need a little more complex offset correction. You have to edit X_POINT_OFFSET (or Y_POINT_OFFSET), as tuple containing an index and a value. Let's say you Y axis is squeezed everywhere, but especialy on the very back part. For a 5 mesh point grid, You have to apply a little offset for the 4 first points (let's say .3mm), and have a more consequent one for the last (let's say .6mm). This can be done setting:
 - Y_POINT_OFFSET 3, 0.3
 - Y_FINAL_OFFSET .6
 - 5 points mesh Y offsets will be: Y0:0, Y1:0.1, Y2:0.2, Y3:0.3, Y4:0.6
