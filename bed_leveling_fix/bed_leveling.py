#!/usr/bin/python3
""" Bed autoleveling software correction """

import os
import sys
from time import sleep
sys.path.append(os.getcwd())
# pylint: disable=C0413
from config import (HANDLE_OCTOPRINT, PREHEAT_BED, PREHEAT_NOZZLE, VERBOSE,
                    BED_TEMP, NOZZLE_TEMP, EEPROM_SAVE, DRY_RUN)  # noqa: E402
from . import correct_leveling_mesh  # noqa: E402
from .printer import PrinterCommand  # noqa: E402

URL = None

if HANDLE_OCTOPRINT:
    import requests
    # pylint: disable=C0412
    from config import OCTO_API, OCTO_API_KEY
    URL = OCTO_API + 'connection?apikey=' + OCTO_API_KEY


def add_output(func, message):
    """ Output log decoration """
    print('{}...'.format(message), end='', flush=True)
    func()
    print('OK')


def main():
    """ Disable octoprint, preheat, home, beg level, apply offsets """
    if HANDLE_OCTOPRINT:
        print('Disabling octoprint connection...', flush=True)
        try:
            requests.post(URL, json={'command': 'disconnect'})
        except Exception:  # pylint: disable=W0703
            print('FAILED')
        sleep(1)
    printer = PrinterCommand()
    if PREHEAT_BED:
        add_output(printer.preheat_bed,
                   'Preheat Bed at {} °C'.format(BED_TEMP))
    if PREHEAT_NOZZLE:
        add_output(printer.preheat_nozzle,
                   'Preheat Nozzle at {} °C'.format(NOZZLE_TEMP))
    add_output(printer.auto_home, 'Home all axis')
    print('Level bed and Apply Leveling offsets', flush=True)
    new_mesh = correct_leveling_mesh(printer.level_bed())
    if not DRY_RUN:
        for mesh_point in new_mesh:
            printer.set_mesh_point(mesh_point[0], mesh_point[1], mesh_point[2])
        if VERBOSE:
            for line in printer.leveling_result.splitlines():
                print(line)
    if EEPROM_SAVE:
        print('Store settings in EEPROM', flush=True)
        printer.save_settings()
    printer.hangup()
    if HANDLE_OCTOPRINT:
        print('Reenabling octoprint connection...', flush=True)
        try:
            requests.post(URL, json={'command': 'connect'})
        except Exception:  # pylint: disable=W0703
            print('FAILED')
    print('All done, bye')
