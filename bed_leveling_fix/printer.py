#!/usr/bin/python3
""" Bed autoleveling software correction
    Printer Interface
"""
from time import sleep
import serial
from config import SERIAL_PORT, SERIAL_BAUDRATE, BED_TEMP, NOZZLE_TEMP


class PrinterInterface:
    """ Handle printer via serial interface """
    def __init__(self):
        print('Initializing printer connection...')
        self._serial = serial.Serial(SERIAL_PORT, SERIAL_BAUDRATE)
        self._wait_for_response()

    def _wait_for_response(self, timeout=10):
        """ Wait until response is given on serial port
            or timeout (seconds) is reached """
        exec_time = 0
        while not self._serial.inWaiting():
            sleep(.2)
            exec_time += .2
            if exec_time > timeout:
                raise TimeoutError

    def _wait_for_confirm(self, timeout=10):
        """ Wait until printer respond 'ok' confirmation string """
        response = b''
        exec_time = 0
        while True:
            response += self._serial.read(size=self._serial.inWaiting())
            if b'ok\n' in response:
                return response
            sleep(1)
            exec_time += .2
            if exec_time > timeout:
                raise TimeoutError

    @property
    def connected(self):
        """ Check connection status """
        if not isinstance(self._serial, serial.Serial):
            return None
        return self._serial.isOpen()

    def send(self, cmd, wait_response=True, confirm=True, timeout=10):
        """ Send gcode command via serial to the printer """
        assert self.connected, 'Connexion error, check your settings'
        response = b''
        self._serial.write(cmd + b'\r\n')
        if wait_response:
            self._wait_for_response(timeout)
            if confirm:
                response = self._wait_for_confirm(timeout)
        self._serial.reset_input_buffer()
        return response

    def hangup(self):
        """ Close printer connection """
        self._serial.close()
        self._serial = None


class PrinterCommand(PrinterInterface):
    """ Printer usefull commands """
    def __init__(self):
        super().__init__()
        self.home_done = False

    @property
    def leveling_result(self):
        """ Get leveling results """
        return self.send(b'M420V1')

    def preheat_bed(self, temp=BED_TEMP):
        """ Preheat bed at BED_TEMP """
        self.send(b'M190S' + to_bytes(temp), timeout=600)

    def preheat_nozzle(self, temp=NOZZLE_TEMP):
        """ Preheat nozzle at BED_TEMP """
        self.send(b'M109S' + to_bytes(temp), timeout=240)

    def preheat(self):
        """ Preheat bed & nozzle """
        self.preheat_bed()
        self.preheat_nozzle()

    def auto_home(self):
        """ Home all axis """
        self.send(b'G28', timeout=30)
        self.home_done = True

    def level_bed(self, get_result=True):
        """ Bed leveling """
        print('Bilinear bed leveling')
        if not self.home_done:
            self.auto_home()
        self.send(b'G29', timeout=900)
        if get_result:
            return self.leveling_result
        return None

    def set_mesh_point(self, x_coord, y_coord, value):
        """ set mesh point value """
        for arg in x_coord, y_coord, value:
            if isinstance(arg, bytes):
                continue
            arg = to_bytes(arg)
        self.send(b'M421I' + to_bytes(x_coord) + b'J' + to_bytes(y_coord) +
                  b'Z' + to_bytes(value))

    def save_settings(self):
        """ Save settings to EEPROM """
        print('Saving results...')
        self.send(b'M500')


def to_bytes(value):
    """ Return value as bytes object """
    return value if isinstance(value, bytes) else str(value).encode('utf-8')
