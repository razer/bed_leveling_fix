#!/usr/bin/python3
""" Bed autoleveling software correction """

import re
import os
import sys
sys.path.append(os.getcwd())
# pylint: disable=C0413
from config import (X_FINAL_OFFSET, X_POINT_OFFSET, Y_FINAL_OFFSET,
                    Y_POINT_OFFSET, VERBOSE)  # noqa: E402

LINE_REGEX = r'^ \d [+-]'


# pylint: disable=R0914,R0912
def correct_leveling_mesh(level_result, verbose=VERBOSE):
    """ return software correction on leveling_result mesh
            level_result : output raw of G29 or M420V1 gcode command
    """
    filtered_level_result = []
    corrected_level_result = []
    first_iteration = True
    if verbose:
        print('Initial bed leveling results:')
    for line in level_result.splitlines():
        if not re.compile(LINE_REGEX).match(line.decode()):
            continue
        filtered_level_result.append(line)
        if verbose:
            print(line)
        if first_iteration:
            first_iteration = False
            grid_size = len(line.split()) - 2
            x_diviser = grid_size - X_POINT_OFFSET[0]
            y_diviser = grid_size - Y_POINT_OFFSET[0]
            x_offset = (X_FINAL_OFFSET - X_POINT_OFFSET[1]) / x_diviser
            y_offset = (Y_FINAL_OFFSET - Y_POINT_OFFSET[1]) / y_diviser
            if verbose:
                print('Grid size:{} - X diviser:{} - Y diviser:{}'.format(
                    grid_size, round(x_diviser, 3), round(y_diviser, 3)))
                print('X offset:{} - Y offset:{}'.format(
                    round(x_offset, 3), round(y_offset, 3)))

    def _get_offset(index, initial_offset, point_offset):
        if index > point_offset[0]:
            return point_offset[1] + (
                initial_offset * (float(index) - point_offset[0]))
        if point_offset[0] and index < point_offset[0]:
            return (point_offset[1]/point_offset[0]) * float(index)
        return point_offset[1]

    if verbose:
        print('index_x  index_y     initial     correction')
    for line in filtered_level_result:
        y_index = line[1:2]
        y_new_offset = _get_offset(int(y_index), y_offset, Y_POINT_OFFSET)
        x_count = 0
        for z_initial in line.split()[1:]:
            x_index = str(x_count).encode('utf-8')
            x_new_offset = _get_offset(int(x_index), x_offset, X_POINT_OFFSET)
            point_offset = x_new_offset + y_new_offset
            z_value = str(round(float(z_initial) + point_offset, 3))
            z_value = z_value.encode('utf-8')
            if verbose:
                print(' {}        {}           {}      {}'.format(
                    x_index.decode(), y_index.decode(),
                    z_initial.decode(), z_value.decode()))
            corrected_level_result.append([int(x_index), int(y_index),
                                           float(z_value)])
            x_count += 1

    return corrected_level_result


def __main__():
    pass
