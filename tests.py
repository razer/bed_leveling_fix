#!/usr/bin/python3
""" Bed autoleveling software correction tests """
import os
import sys
sys.path.append(os.getcwd())
# pylint: disable=C0413
import bed_leveling_fix  # noqa: E402

LEVEL_RESULT1 = (b'Bilinear Leveling Grid:\n'
                 + b'      0      1      2      3      4\n'
                 + b' 0 -0.043 +0.029 -0.022 -0.026 -0.062\n'
                 + b' 1 +0.060 +0.199 -0.022 +0.049 -0.132\n'
                 + b' 2 +0.010 +0.129 +0.112 -0.016 -0.127\n'
                 + b' 3 +0.055 +0.079 +0.020 +0.086 -0.025\n'
                 + b' 4 +0.093 +0.159 +0.111 -0.031 -0.020\n\n'
                 + b' echo:Bed Leveling Off\necho:Fade Height Off\nok\n')

LEVEL_RESULT2 = (b'Bilinear Leveling Grid:\n'
                 + b'      0      1      2      3      4      5      6\n'
                 + b' 0 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000\n'
                 + b' 1 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000\n'
                 + b' 2 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000\n'
                 + b' 3 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000\n'
                 + b' 4 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000\n'
                 + b' 5 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000\n'
                 + b' 6 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000 +0.000\n\n'
                 + b' echo:Bed Leveling Off\necho:Fade Height Off\nok\n')

# BED_LEVELING.correct_leveling_mesh(level_result=LEVEL_RESULT1, dryrun=True)


# pylint: disable=R0903
class Test:
    """ Test generic class """
    X_POINT_OFFSET = tuple()
    X_FINAL_OFFSET = None
    Y_POINT_OFFSET = tuple()
    Y_FINAL_OFFSET = None
    LEVEL_RESULT = None
    EXPECTED = []

    @classmethod
    def run(cls):
        """ Run the test """
        bed_leveling_fix.X_POINT_OFFSET = cls.X_POINT_OFFSET
        bed_leveling_fix.X_FINAL_OFFSET = cls.X_FINAL_OFFSET
        bed_leveling_fix.Y_POINT_OFFSET = cls.Y_POINT_OFFSET
        bed_leveling_fix.Y_FINAL_OFFSET = cls.Y_FINAL_OFFSET
        result = bed_leveling_fix.correct_leveling_mesh(cls.LEVEL_RESULT,
                                                        verbose=False)
        # print(result)
        assert result == cls.EXPECTED, 'Something goes wrong'


# pylint: disable=R0903
class Test0(Test):
    """ Test 1 """
    # X_INITIAL_OFFSET = 1.00
    X_POINT_OFFSET = 0, 0
    X_FINAL_OFFSET = 0
    Y_POINT_OFFSET = 2, 1
    Y_FINAL_OFFSET = 5
    LEVEL_RESULT = LEVEL_RESULT2
    # pylint: disable=C0301
    EXPECTED = [[0, 0, 0.0], [1, 0, 0.0], [2, 0, 0.0], [3, 0, 0.0], [4, 0, 0.0], [5, 0, 0.0], [6, 0, 0.0],  # noqa: E501
                [0, 1, 0.5], [1, 1, 0.5], [2, 1, 0.5], [3, 1, 0.5], [4, 1, 0.5], [5, 1, 0.5], [6, 1, 0.5],  # noqa: E501
                [0, 2, 1.0], [1, 2, 1.0], [2, 2, 1.0], [3, 2, 1.0], [4, 2, 1.0], [5, 2, 1.0], [6, 2, 1.0],  # noqa: E501
                [0, 3, 2.0], [1, 3, 2.0], [2, 3, 2.0], [3, 3, 2.0], [4, 3, 2.0], [5, 3, 2.0], [6, 3, 2.0],  # noqa: E501
                [0, 4, 3.0], [1, 4, 3.0], [2, 4, 3.0], [3, 4, 3.0], [4, 4, 3.0], [5, 4, 3.0], [6, 4, 3.0],  # noqa: E501
                [0, 5, 4.0], [1, 5, 4.0], [2, 5, 4.0], [3, 5, 4.0], [4, 5, 4.0], [5, 5, 4.0], [6, 5, 4.0],  # noqa: E501
                [0, 6, 5.0], [1, 6, 5.0], [2, 6, 5.0], [3, 6, 5.0], [4, 6, 5.0], [5, 6, 5.0], [6, 6, 5.0]]  # noqa: E501


# pylint: disable=R0903
class Test1(Test):
    """ Test 1 """
    # X_INITIAL_OFFSET = 1.00
    X_POINT_OFFSET = 2, 1
    X_FINAL_OFFSET = 5
    Y_POINT_OFFSET = 0, 0
    Y_FINAL_OFFSET = 0
    LEVEL_RESULT = LEVEL_RESULT2
    # pylint: disable=C0301
    EXPECTED = [[0, 0, 0.0], [1, 0, 0.5], [2, 0, 1.0], [3, 0, 2.0], [4, 0, 3.0], [5, 0, 4.0], [6, 0, 5.0],  # noqa: E501
                [0, 1, 0.0], [1, 1, 0.5], [2, 1, 1.0], [3, 1, 2.0], [4, 1, 3.0], [5, 1, 4.0], [6, 1, 5.0],  # noqa: E501
                [0, 2, 0.0], [1, 2, 0.5], [2, 2, 1.0], [3, 2, 2.0], [4, 2, 3.0], [5, 2, 4.0], [6, 2, 5.0],  # noqa: E501
                [0, 3, 0.0], [1, 3, 0.5], [2, 3, 1.0], [3, 3, 2.0], [4, 3, 3.0], [5, 3, 4.0], [6, 3, 5.0],  # noqa: E501
                [0, 4, 0.0], [1, 4, 0.5], [2, 4, 1.0], [3, 4, 2.0], [4, 4, 3.0], [5, 4, 4.0], [6, 4, 5.0],  # noqa: E501
                [0, 5, 0.0], [1, 5, 0.5], [2, 5, 1.0], [3, 5, 2.0], [4, 5, 3.0], [5, 5, 4.0], [6, 5, 5.0],  # noqa: E501
                [0, 6, 0.0], [1, 6, 0.5], [2, 6, 1.0], [3, 6, 2.0], [4, 6, 3.0], [5, 6, 4.0], [6, 6, 5.0]]  # noqa: E501


# pylint: disable=R0903
class Test2(Test):
    """ Test 1 """
    # X_INITIAL_OFFSET = 1.00
    X_POINT_OFFSET = 1, 1
    X_FINAL_OFFSET = 3
    Y_POINT_OFFSET = 1, -1
    Y_FINAL_OFFSET = -3.50
    LEVEL_RESULT = LEVEL_RESULT2
    # pylint: disable=C0301
    EXPECTED = [[0, 0, 0.0], [1, 0, 1.0], [2, 0, 1.4], [3, 0, 1.8], [4, 0, 2.2], [5, 0, 2.6], [6, 0, 3.0],  # noqa: E501
                [0, 1, -1.0], [1, 1, 0.0], [2, 1, 0.4], [3, 1, 0.8], [4, 1, 1.2], [5, 1, 1.6], [6, 1, 2.0],  # noqa: E501
                [0, 2, -1.5], [1, 2, -0.5], [2, 2, -0.1], [3, 2, 0.3], [4, 2, 0.7], [5, 2, 1.1], [6, 2, 1.5],  # noqa: E501
                [0, 3, -2.0], [1, 3, -1.0], [2, 3, -0.6], [3, 3, -0.2], [4, 3, 0.2], [5, 3, 0.6], [6, 3, 1.0],  # noqa: E501
                [0, 4, -2.5], [1, 4, -1.5], [2, 4, -1.1], [3, 4, -0.7], [4, 4, -0.3], [5, 4, 0.1], [6, 4, 0.5],  # noqa: E501
                [0, 5, -3.0], [1, 5, -2.0], [2, 5, -1.6], [3, 5, -1.2], [4, 5, -0.8], [5, 5, -0.4], [6, 5, 0.0],  # noqa: E501
                [0, 6, -3.5], [1, 6, -2.5], [2, 6, -2.1], [3, 6, -1.7], [4, 6, -1.3], [5, 6, -0.9], [6, 6, -0.5]]  # noqa: E501


# pylint: disable=R0903
class Test3(Test):
    """ Test 1 """
    # X_INITIAL_OFFSET = 1.00
    X_POINT_OFFSET = 0, 0
    X_FINAL_OFFSET = 6
    Y_POINT_OFFSET = 0, 0
    Y_FINAL_OFFSET = 0
    LEVEL_RESULT = LEVEL_RESULT2
    # pylint: disable=C0301
    EXPECTED = [[0, 0, 0.0], [1, 0, 1.0], [2, 0, 2.0], [3, 0, 3.0], [4, 0, 4.0], [5, 0, 5.0], [6, 0, 6.0],  # noqa: E501
                [0, 1, 0.0], [1, 1, 1.0], [2, 1, 2.0], [3, 1, 3.0], [4, 1, 4.0], [5, 1, 5.0], [6, 1, 6.0],  # noqa: E501
                [0, 2, 0.0], [1, 2, 1.0], [2, 2, 2.0], [3, 2, 3.0], [4, 2, 4.0], [5, 2, 5.0], [6, 2, 6.0],  # noqa: E501
                [0, 3, 0.0], [1, 3, 1.0], [2, 3, 2.0], [3, 3, 3.0], [4, 3, 4.0], [5, 3, 5.0], [6, 3, 6.0],  # noqa: E501
                [0, 4, 0.0], [1, 4, 1.0], [2, 4, 2.0], [3, 4, 3.0], [4, 4, 4.0], [5, 4, 5.0], [6, 4, 6.0],  # noqa: E501
                [0, 5, 0.0], [1, 5, 1.0], [2, 5, 2.0], [3, 5, 3.0], [4, 5, 4.0], [5, 5, 5.0], [6, 5, 6.0],  # noqa: E501
                [0, 6, 0.0], [1, 6, 1.0], [2, 6, 2.0], [3, 6, 3.0], [4, 6, 4.0], [5, 6, 5.0], [6, 6, 6.0]]  # noqa: E501


# pylint: disable=R0903
class Test4(Test):
    """ Test 1 """
    # X_INITIAL_OFFSET = 1.00
    X_POINT_OFFSET = 3, 3
    X_FINAL_OFFSET = 0
    Y_POINT_OFFSET = 0, 0
    Y_FINAL_OFFSET = 0
    LEVEL_RESULT = LEVEL_RESULT2
    # pylint: disable=C0301
    EXPECTED = [[0, 0, 0.0], [1, 0, 1.0], [2, 0, 2.0], [3, 0, 3.0], [4, 0, 2.0], [5, 0, 1.0], [6, 0, 0.0],  # noqa: E501
                [0, 1, 0.0], [1, 1, 1.0], [2, 1, 2.0], [3, 1, 3.0], [4, 1, 2.0], [5, 1, 1.0], [6, 1, 0.0],  # noqa: E501
                [0, 2, 0.0], [1, 2, 1.0], [2, 2, 2.0], [3, 2, 3.0], [4, 2, 2.0], [5, 2, 1.0], [6, 2, 0.0],  # noqa: E501
                [0, 3, 0.0], [1, 3, 1.0], [2, 3, 2.0], [3, 3, 3.0], [4, 3, 2.0], [5, 3, 1.0], [6, 3, 0.0],  # noqa: E501
                [0, 4, 0.0], [1, 4, 1.0], [2, 4, 2.0], [3, 4, 3.0], [4, 4, 2.0], [5, 4, 1.0], [6, 4, 0.0],  # noqa: E501
                [0, 5, 0.0], [1, 5, 1.0], [2, 5, 2.0], [3, 5, 3.0], [4, 5, 2.0], [5, 5, 1.0], [6, 5, 0.0],  # noqa: E501
                [0, 6, 0.0], [1, 6, 1.0], [2, 6, 2.0], [3, 6, 3.0], [4, 6, 2.0], [5, 6, 1.0], [6, 6, 0.0]]  # noqa: E501


# pylint: disable=R0903
class Test5(Test):
    """ Test 1 """
    # X_INITIAL_OFFSET = 1.00
    X_POINT_OFFSET = 0, 0
    X_FINAL_OFFSET = 0
    Y_POINT_OFFSET = 3, 3
    Y_FINAL_OFFSET = 0
    LEVEL_RESULT = LEVEL_RESULT2
    # pylint: disable=C0301
    EXPECTED = [[0, 0, 0.0], [1, 0, 0.0], [2, 0, 0.0], [3, 0, 0.0], [4, 0, 0.0], [5, 0, 0.0], [6, 0, 0.0],  # noqa: E501
                [0, 1, 1.0], [1, 1, 1.0], [2, 1, 1.0], [3, 1, 1.0], [4, 1, 1.0], [5, 1, 1.0], [6, 1, 1.0],  # noqa: E501
                [0, 2, 2.0], [1, 2, 2.0], [2, 2, 2.0], [3, 2, 2.0], [4, 2, 2.0], [5, 2, 2.0], [6, 2, 2.0],  # noqa: E501
                [0, 3, 3.0], [1, 3, 3.0], [2, 3, 3.0], [3, 3, 3.0], [4, 3, 3.0], [5, 3, 3.0], [6, 3, 3.0],  # noqa: E501
                [0, 4, 2.0], [1, 4, 2.0], [2, 4, 2.0], [3, 4, 2.0], [4, 4, 2.0], [5, 4, 2.0], [6, 4, 2.0],  # noqa: E501
                [0, 5, 1.0], [1, 5, 1.0], [2, 5, 1.0], [3, 5, 1.0], [4, 5, 1.0], [5, 5, 1.0], [6, 5, 1.0],  # noqa: E501
                [0, 6, 0.0], [1, 6, 0.0], [2, 6, 0.0], [3, 6, 0.0], [4, 6, 0.0], [5, 6, 0.0], [6, 6, 0.0]]  # noqa: E501


for test in Test1, Test2, Test3, Test4, Test5:
    try:
        print('Running leveling correction {}...'.format(test.__name__),
              end='')
        test.run()
    except AssertionError:
        print('ERROR')
    else:
        print('OK')
